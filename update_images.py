import csv

with open('gene_with_image_details.csv') as csvfile:
    csv_reader = csv.reader(csvfile)
    next(csv_reader)
    response = filter(lambda x: x[7] == 'yes', csv_reader)
    
output = []
base_gene_condition = 'https://invitae.imgix.net/web/gene-condition/'

for item in response:
    output.append(str(base_gene_condition + item[1] + '.png'))

base_graphics = 'https://invitae.imgix.net/web/graphics/'

graphics = ['XL', 'AR', 'AD']
[output.append(str(base_graphics + graphic + '.svg')) for graphic in graphics]
print(output)