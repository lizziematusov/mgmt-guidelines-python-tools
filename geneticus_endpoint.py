import csv
import requests
import json
import sys
import os

from invitae_rendering_service_client import (ApiClient, Configuration, DefaultApi)
url = 'http://geneticus-prd.vpc.locusdev.net/api/v4/management_guidelines/'

def add_inheritance_and_interpretation_info(gene_id, gene_name, is_vid_flyer):
    # Type: [String] -> Dict[String, String]
    gene_inheritance_data = {
        "gene_inheritance": {
            "flyerPositive": gene_id_to_inheritance_info[gene_id][0],
            "strongEvidence": gene_id_to_inheritance_info[gene_id][1]
        }
    }
    increased_risk_flyers = ['APC', 'HOXB13']
    if is_vid_flyer and gene_name in increased_risk_flyers:
        gene_inheritance_data["geneInterpretation"] = 'Increased Risk'
    else:
        gene_inheritance_data["geneInterpretation"] = 'Positive'
    return gene_inheritance_data

def convert_guidelines_and_recommendation(guidelines_and_recommendation):
    # Type: List[Dict[String, String]] -> List[Dict[String,String]]
    converted_guidelines = []
    for guideline_set in guidelines_and_recommendation:
        converted_dict = {}
        for key in guideline_set.keys():
            converted_dict[to_camel_case(key)] = guideline_set.pop(key)
        converted_guidelines.append(converted_dict)
    return converted_guidelines

def to_camel_case(snake_str):
    # Type: [String] -> [String]
    components = snake_str.split('_')
    return components[0] + ''.join(x.title() for x in components[1:])

# #####
# # take a CSV file and turn it into a geneticus payload
# #####
gene_list = []
gene_id_to_inheritance_info = {}
with open('geneticus_input_csv.csv') as csvfile:
    csv_reader = csv.reader(csvfile)
    next(csv_reader)
    for row in csv_reader:
        gene_dict = {}
        gene_dict["invitae_gene_id"] = row[1]   
        gene_dict["vids"] = [{"vid": row[4], "zygosity": row[5]}]
        gene_dict["flyer_positive"] = row[2]
        gene_dict["strong_evidence"] = row[3].split('/') #right now we only get 1 val for strong_evidence, need to update if we want to support [AD,AR]
        gene_id_to_inheritance_info[row[1]] = [row[2], row[3].split('/')] #row 1 is invitae_gene_id, row 2 is flyer_positive row 3 is strong_evidence
        gene_list.append(gene_dict)

gene_dictionary = {"genes": gene_list}
dumped_gene_dictionary = json.dumps(gene_dictionary)
json_gene_dictionary = json.loads(dumped_gene_dictionary)

# ####
# # Call geneticus with that payload
# ####

response_json = requests.post(url, json=json_gene_dictionary, headers={'Content-Type':'application/json'})
response_obj = json.loads(response_json.text)

flyers_not_rendered = []
flyers_rendered = []
pdf_payloads = []

####
# Create a filtered list of payloads based on geneticus should_render_flyer boolean
####
for obj in response_obj['management_guidelines_texts']:
    if obj['should_render_flyer'] is True:
        flyers_rendered.append(obj['gene_name'])
        obj.update(add_inheritance_and_interpretation_info(obj['invitae_gene_id'], obj['gene_name'], obj['is_vid_flyer']))
        pdf_payloads.append(obj)
    elif obj['should_render_flyer'] is False:
        flyers_not_rendered.append(obj['gene_name'])

####
# Convert from snake case to camel case
####    
list_of_payloads = []
for pdf in pdf_payloads:
    formatted_payload = {}
    for key in pdf.keys():
        if key == 'guidelines_and_recommendation':
            json_formatted_guidelines = convert_guidelines_and_recommendation(pdf[key])
            
            formatted_payload[to_camel_case(key)] = json_formatted_guidelines
        elif key != 'should_render_flyer':
            formatted_payload[to_camel_case(key)] = pdf.pop(key)
    list_of_payloads.append(formatted_payload)
print('Flyers rendered: ', flyers_rendered)
print('Flyers not rendered: ', flyers_not_rendered)

####
# Configure local swagger client of rendering service
#### 
configuration = Configuration()
configuration.host = 'http://localhost:5000'
api_client = ApiClient(configuration=configuration)
api = DefaultApi(api_client=api_client)

for payload in list_of_payloads:
    temp_pdf_filename = api.generate_management_guidelines_flyer_pdf(data=payload)
    if payload['isVidFlyer'] == True:
        output_name = '{}-{}.pdf'.format(payload['geneName'], payload['vids'][0]['vid'])
    else:
        output_name = '{}.pdf'.format(payload['geneName'])
    full_output_path = os.path.join('/Users/lizziematusov/Repositories/mgmt-guidelines-python-tools/finished_flyers', output_name)
    with open(temp_pdf_filename, 'rb') as temp_pdf, open(full_output_path, 'wb') as pdf_file:
        pdf_file.write(temp_pdf.read())
